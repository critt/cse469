#!/usr/bin/python3

import argparse



#create argument parser
argParser = argparse.ArgumentParser()
options = argParser.add_mutually_exclusive_group(required=True)
choices = argParser.add_mutually_exclusive_group(required=True)
#one of these 3 options needs to be provided
options.add_argument('-L', '--logical', action='store_true')
options.add_argument('-P', '--physical', action='store_true')
options.add_argument('-C', '--cluster', action='store_true')
argParser.add_argument('-b', '--partition-start', metavar='offset', type=int, default=0)
argParser.add_argument('-B', '--byte-address', action='store_true')
#default sector size is 512 unless otherwise specified
argParser.add_argument('-s', '--sector-size', metavar='bytes', type=int, default=512)
#one of these 3 addresses must be provided
choices.add_argument('-l', '--logical-known', metavar='address', type=int)
choices.add_argument('-p', '--physical-known', metavar='address', type=int)
choices.add_argument('-c', '--cluster-known', metavar='address', type=int)
argParser.add_argument('-k', '--cluster-size', metavar='sectors', type=int)
argParser.add_argument('-r', '--reserved', metavar='sectors', type=int)
argParser.add_argument('-t', '--fat-tables', metavar='tables', type=int)
argParser.add_argument('-f', '--fat-length', metavar='sectors', type=int)
#get all arguments
args = argParser.parse_args()
try:
	physicalAddr = None
	newAddr = None
	#convert the given address to the physical address first
	#to make conversions easier
	if args.physical_known:
		physicalAddr = args.physical_known
	elif args.logical_known:
		physicalAddr = args.partition_start + args.logical_known
	elif args.cluster_known:
		if args.cluster_size is None or args.reserved is None or args.fat_tables is None or args.fat_length is None:
			raise Exception('all arguments -k, -r, -t and -f need to be given when the cluster address is provided.')
		physicalAddr = args.partition_start + ((args.cluster_known - 2) * args.cluster_size) + args.reserved + (args.fat_tables * args.fat_length)
	#convert to proper address based on given choice
	if args.physical:
		newAddr = physicalAddr
	elif args.logical:
		newAddr = physicalAddr - args.partition_start
	elif args.cluster:
		newAddr = ((physicalAddr - (args.partition_start + args.reserved + (args.fat_tables * args.fat_length))) // args.cluster) + 2
	#convert address to bytes if the -B argument is given
	if args.byte_address:
		newAddr = newAddr * args.sector_size
	print(newAddr)
except Exception as ex:
	print(str(ex))
