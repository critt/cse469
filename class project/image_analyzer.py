#!/usr/bin/python3

import argparse
import sys
import os.path
import hashlib
import struct
import math

#Helper function for getting a partition type string from hex
def returnType(s):
	if s == 0x01:
		return 'DOS 12-bit FAT'
	if s == 0x04:
		return 'DOS 16-bit FAT for partitions smaller than 32MB'
	if s == 0x05:
		return 'Extended partition'
	if s == 0x06:
		return 'DOS 16-bit FAT for partitions larger than 32MB'
	if s == 0x07:
		return 'NTFS'
	if s == 0x08:
		return 'AIX bootable partition'
	if s == 0x09:
		return 'AIX data partition'
	if s == 0x0B:
		return 'DOS 32-bit FAT'
	if s == 0x0C:
		return 'DOS 32-bit FAT for interrupt 13 support'
	if s == 0x17:
		return 'Hidden NTFS partition (XP and earlier)'
	if s == 0x1B:
		return 'Hidden FAT32 partition'
	if s == 0x1E:
		return 'Hidden VFAT partition'
	if s == 0x3C:
		return 'Partition Magic recovery partition'
	if s == 0x66:
		return 'Novell partitions'
	if s == 0x67:
		return 'Novell partitions'
	if s == 0x68:
		return 'Novell partitions'
	if s == 0x69:
		return 'Novell partitions'
	if s == 0x81:
		return 'Linux'
	if s == 0x82:
		return 'Linux swap partition (can also be associated with Solaris partitions)'
	if s == 0x83:
		return 'Linux native file systems (Ext2, Ext3, Reiser, xiafs)'
	if s == 0x86:
		return 'FAT16 volume/stripe set (Windows NT)'
	if s == 0x87:
		return 'High Performance File System (HPFS) fault-tolerant mirrored partition or NTFS volume/strip set'
	if s == 0xA5:
		return 'FreeBSD and BSD/386'
	if s == 0xA6:
		return 'OpenBSD'
	if s == 0xA9:
		return 'NetBSD'
	if s == 0xC7:
		return 'Typical of a corrupted NTFS volume/stripe set'
	if s == 0xEB:
		return 'BeOS'
	if s == 0x00:
		return 'empty'
	else:
		return None



#Initialize parser
parser = argparse.ArgumentParser()
parser.add_argument('path')
args = parser.parse_args()
hmd5 = hashlib.md5()
hsha1 = hashlib.sha1()



#Print MD5 hash, write hash to file
with open(args.path, 'rb') as source:
	buf = source.read()
	hmd5.update(buf)
	print()
	print('Checksums:')
	print('==================================================')
	print('MD5: ' + hmd5.hexdigest() + '\n')
	fileName, fileExtension = os.path.splitext(args.path)
	prefix = 'MD5-'
	suffix = '.txt'
	with open(fileName.join((prefix, suffix)), 'w') as dest:
		dest.write(hmd5.hexdigest())



#Print SHA1 hash, write hash to file
with open(args.path, 'rb') as source:
	buf = source.read()
	hsha1.update(buf)
	print('SHA1: ' + hsha1.hexdigest())
	print('==================================================')
	fileName, fileExtension = os.path.splitext(args.path)
	prefix = 'SHA1-'
	suffix = '.txt'
	with open(fileName.join((prefix, suffix)), 'w') as dest:
		dest.write(hsha1.hexdigest())



#Locate and extract the partition tables from the MBR
with open(args.path, 'rb') as source:
	#for all four partition entries
	for i in range(0,4):
		#get type in hex and string
		source.seek(0x01BE + (i*16) + 0x04)
		chunk = source.read(1)
		typeInt = int.from_bytes(chunk, byteorder="little")
		typeHex = "%0.2x" % typeInt
		typeStr = returnType(typeInt)

		#get starting address
		source.seek(0x01BE + (i*16) + 0x08)
		chunk = source.read(4)
		startAddr = int.from_bytes(chunk, byteorder="little")

		#get partition size
		source.seek(0x01BE + (i*16) + 0x0C)
		chunk = source.read(4)
		partitionSize = int.from_bytes(chunk, byteorder="little")

		#print
		if typeStr == None:
			print("Unexpected partition type")
		elif typeStr == 'empty':
			pass
		else:
			print("(" + typeHex.zfill(2) + ") " + typeStr + ", " + str(startAddr).zfill(10) + ", " + str(partitionSize).zfill(10))




#For FAT16/32 partitions, read each partition's VBR and retrieve the geometric data of the file system
with open(args.path, 'rb') as source:
	#for all four partition entries
	for i in range(0,4):
		#get type in hex and string
		source.seek(0x01BE + (i*16) + 0x04)
		chunk = source.read(1)
		typeInt = int.from_bytes(chunk, byteorder="little")
		typeHex = "%0.2x" % typeInt
		typeStr = returnType(typeInt)

		#for FAT16 and FAT32 only
		if typeInt == 0x04 or typeInt == 0x06 or typeInt == 0x0B or typeInt == 0x0C :
			print("==================================================")

			#get starting address
			source.seek(0x01BE + (i*16) + 0x08)
			chunk = source.read(4)
			startAddr = int.from_bytes(chunk, byteorder="little")
			offset = (startAddr * 512)

			#Find size in sectors of reserved area
			source.seek(offset + 14)
			chunk1 = source.read(2)
			reservedAreaSize = int.from_bytes(chunk1, byteorder="little")
			
			#Find sectors per cluster
			source.seek(offset + 13)
			chunk2 = source.read(1)
			sectorsPerCluster = int.from_bytes(chunk2, byteorder="little")
			
			#Find size of each FAT
			#32-bit offset and read length
			is32 = True
			if typeInt == 0x0B or typeInt == 0x0C :
				tmpOffset = offset + 36
				tmpReadLength = 4
			#16-bit offset and read length
			else:
				tmpOffset = offset + 22
				tmpReadLength = 2
				is32 = False
			source.seek(tmpOffset)
			chunk3 = source.read(tmpReadLength)
			sizeOfEachFat = int.from_bytes(chunk3, byteorder="little")
			
			#Find number of FATs
			source.seek(offset + 16)
			chunk4 = source.read(1)
			numFats = int.from_bytes(chunk4, byteorder="little")
			
			#Find FAT area start sector
			fatAreaStartSector = reservedAreaSize
			
			#Find FAT area ending sector
			fatAreaEndingSector = (sizeOfEachFat * numFats) + fatAreaStartSector - 1
			
			#Find first sector of cluster 2
			source.seek(offset + 28)
			chunk5 = source.read(4)
			sectorsBeforeStartOfPartition = int.from_bytes(chunk5, byteorder="little")
			#32-bit
			if is32:
				firstSectorOfCluster2 = sectorsBeforeStartOfPartition + fatAreaEndingSector + 1
			#16-bit
			else:
				source.seek(offset + 17)
				chunk6 = source.read(2)
				maxFilesInRootDirectory = int.from_bytes(chunk6, byteorder="little")
				firstSectorOfCluster2 = sectorsBeforeStartOfPartition + fatAreaEndingSector + 1 + int(math.ceil(maxFilesInRootDirectory) / 16)

			#print
			print("Partition " + str(i) + "(" + typeStr + "):")
			print("Reserved area: Start sector: 0 Ending sector: " + str(reservedAreaSize - 1) + " Size: " + str(reservedAreaSize) + " sectors")
			print("Sectors per cluster: " + str(sectorsPerCluster) + " sectors")
			print("FAT area: Start sector: " + str(fatAreaStartSector) + " Ending sector: " + str(fatAreaEndingSector))
			print("# of FATs: " + str(numFats))
			print("The size of each FAT: " + str(sizeOfEachFat) + " sectors")
			print("The first sector of cluster 2: " + str(firstSectorOfCluster2) + " sectors")


			

			

