#!/usr/bin/python3

import argparse

argParser = argparse.ArgumentParser()
choice = argParser.add_mutually_exclusive_group(required=True)
choice.add_argument('-T', '--time', action='store_true')
choice.add_argument('-D', '--date', action='store_true')

inType = argParser.add_mutually_exclusive_group(required=True)
inType.add_argument('-f', '--filename', metavar='filename')
inType.add_argument('-x', '--hex-value', metavar='hex value', type=str)

args = argParser.parse_args()

try:
	hexValue = None

	if args.hex_value:
		hexValue = int(args.hex_value, 16)
	elif args.filename:
		file = open(args.filename, 'r')
		hexValue = int(file.read(), 16)

	#converts hex value from big endian to little endian
	hexValue = (hexValue << 8) | (hexValue >> 8)

	if args.time:
		sec = (hexValue & 0x1f) * 2
		minute = (hexValue >> 5) & 0x3f
		hour = 	(hexValue >> 11) & 0x1f
		amOrPm = 'AM'

		#check valid input
		if hour > 23:
			raise Exception('Invalid input the range for hours should be 0-23')
		if minute > 59:
			raise Exception('Invalid input the range for minutes should be 0-59')
		if sec > 58:
			raise Exception('Invalid input the range for seconds should be 0-29 as that value is doubled')

		if hour >= 12:
			amOrPm = 'PM'
			hour = hour - 12

		if hour == 0:
			hour = 12

		print('Time: {0}:{1}:{2} {3}'.format(hour, minute, sec, amOrPm))

	elif args.date:
		day = hexValue & 0x1f
		month = (hexValue >> 5) & 0x0f
		year = 1980 + ((hexValue >> 9) & 0x7f)

		if month > 12 | month == 0:
			raise Exception('Invalid input the range for months should be 1-12')

		if month == 1:
			month = 'Jan'
		elif month == 2:
			month = 'Feb'
		elif month == 3:
			month = 'Mar'
		elif month == 4:
			month = 'Apr'
		elif month == 5:
			month = 'May'
		elif month == 6:
			month = 'Jun'
		elif month == 7:
			month = 'Jul'
		elif month == 8:
			month = 'Aug'
		elif month == 9:
			month = 'Sep'
		elif month == 10:
			month = 'Oct'
		elif month == 11:
			month = 'Nov'
		elif month == 12:
			month = 'Dec'

		print('Date: {0} {1}, {2}'.format(month, day, year))

except Exception as ex:
	print(str(ex))